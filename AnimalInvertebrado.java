
class AnimalInvertebrado extends Animal{

  private boolean locomoção;
  private String reproducao;

  void imprimeDados(){

	if(locomoção == false)
	  System.out.println("Animal invertebrado com reprodução " + reproducao + " com pouca ou nenhuma locomoção.");
	
	else
	  System.out.println("Animal invertebrado com reprodução " + reproducao + " com locomoção.");

}

}
