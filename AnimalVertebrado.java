
class AnimalVertebrado extends Animal{

  private String tipoMusculos;
  private String respiratorio;

  void imprimeDados(){

	System.out.println("Esse animal vertebrado apresenta musculatura do tipo " + tipoMusculos + " e sistema respiratório através de " + respiratorio + ".");
  }
}
